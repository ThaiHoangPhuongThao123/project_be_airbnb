import { Body, Controller, Delete, Get, Param, Post, Put, Req, UseGuards } from '@nestjs/common';
import { CommentService } from './comment.service';
import { BinhLuan } from '@prisma/client';
import { ApiBearerAuth, ApiProperty, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';

class Comment {
  @ApiProperty()
  ma_phong: Number;
  @ApiProperty()
  ngay_binh_luan: Date;
  @ApiProperty()
  noi_dung: String;
   @ApiProperty()
  sao_binh_luan: Number;
}

@ApiTags('Comment')
@Controller('comment')
export class CommentController {
  constructor(private readonly commentService: CommentService) { }
  @Get('/comments')
  getComments(): Promise<BinhLuan[]> {
    return this.commentService.getComments();
  }
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/comment')
  postComment(@Req() req, @Body() body: Comment) {
    let { data } = req.user;
    return this.commentService.postComment(data, body);
  }

    @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Put('/comment/:id')
  updateCommentById(
    @Req() req,
    @Param('id') id: Number,
    @Body() body: Comment,
  ) {
    let { data } = req.user;
    id = Number(id);
    return this.commentService.updateCommentById(data, id, body);
  }
  
   @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete('/comment/:id')
  deleteCommentById(@Req() req, @Param('id') id: Number) {
    let { data } = req.user;
    id = Number(id);
    return this.commentService.deleteCommentById(data, id);
   }
  
   @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/comment/:idRoom')
  getCommentByIdRoom(@Req() req, @Param('idRoom') idRoom: Number) {
    let { data } = req.user;
    idRoom = Number(idRoom);
    return this.commentService.getCommentByIdRoom(data, idRoom);
  }
}
