import {
  HttpException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { BinhLuan, PrismaClient } from '@prisma/client';

@Injectable()
export class CommentService {
  prisma = new PrismaClient();

  async getComments(): Promise<BinhLuan[]> {
    let comments = await this.prisma.binhLuan.findMany();
    return comments;
  }

  async postComment(data, body) {
    try {
      let {
        ma_phong,
        ngay_binh_luan,
        noi_dung,
        sao_binh_luan,
      } = body;
      if (!data) {
        throw new HttpException('Unauthorized ', 401);
      }
      let newComment = await this.prisma.binhLuan.create({
        data: {
          ma_phong,
          ma_nguoi_binh_luan: data.id,
          ngay_binh_luan,
          noi_dung,
          sao_binh_luan,
        },
      });
      return newComment;
    } catch (exception) {
      if (exception.status === 401) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async updateCommentById(data, id, body) {
    try {
      let {
        ma_phong,
        ngay_binh_luan,
        noi_dung,
        sao_binh_luan,
      } = body;
      if (!data) {
        throw new HttpException('Unauthorized ', 401);
      }
      let comment = await this.prisma.binhLuan.findUnique({
        where: {
          id: id,
        },
      });

      if (!comment) {
        throw new HttpException('Resource not found.', 404);
      }
      let newComment = await this.prisma.binhLuan.update({
        where: {
          id: id,
        },
        data: {
          ma_phong,
          ngay_binh_luan,
          noi_dung,
          sao_binh_luan,
        },
      });

      return newComment;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async deleteCommentById(data, id) {
    try {
      if (!data) {
        throw new HttpException('Unauthorized ', 401);
      }
      let comment = await this.prisma.binhLuan.findUnique({
        where: {
          id: id,
        },
      });
      if (!comment) {
        throw new HttpException('Resource not found.', 404);
      }
      await this.prisma.binhLuan.delete({
        where: {
          id: id,
        },
      });
      return 'Deleted successfully';
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getCommentByIdRoom(data, idRoom) {
    try {
      if (!data) {
        throw new HttpException('Unauthorized ', 401);
      }
      let comments = await this.prisma.binhLuan.findMany({
        where: {
          ma_phong: idRoom,
        },
      });
      if (comments.length == 0) {
        throw new HttpException('Resource not found.', 404);
      }
      return comments;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }
}
