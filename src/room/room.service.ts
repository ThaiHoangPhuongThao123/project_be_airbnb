import {
  HttpException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { Phong, PrismaClient } from '@prisma/client';

@Injectable()
export class RoomService {
  prisma = new PrismaClient();

  async getRooms(): Promise<Phong[]> {
    let rooms = await this.prisma.phong.findMany();
    return rooms;
  }

  async postRoom(data, body) {
    try {
      let { role } = data;
      let {
        ten_phong,
        khach,
        phong_ngu,
        giuong,
        phong_tam,
        mo_ta,
        gia_tien,
        may_giat,
        ban_la,
        tivi,
        dieu_hoa,
        wifi,
        bep,
        do_xe,
        ho_boi,
        ban_ui,
        ma_vi_tri,
        hinh_anh,
      } = body;
      if (role !== 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let location = await this.prisma.viTri.findUnique({
        where: {
          id: ma_vi_tri,
        },
      });
      if (!location) {
        throw new HttpException('"ma-vi-tri" not found..', 404);
      }
      await this.prisma.phong.create({
        data: {
          ten_phong: ten_phong,
          khach: khach,
          phong_ngu: phong_ngu,
          giuong: giuong,
          phong_tam: phong_tam,
          mo_ta: mo_ta,
          gia_tien: gia_tien,
          may_giat: may_giat,
          ban_la: ban_la,
          tivi: tivi,
          dieu_hoa: dieu_hoa,
          wifi: wifi,
          bep: bep,
          do_xe: do_xe,
          ho_boi: ho_boi,
          ban_ui: ban_ui,
          ma_vi_tri: ma_vi_tri,
          hinh_anh: hinh_anh,
        },
      });

      return 'Added successfully';
    } catch (exception) {
      if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getRoomsByIdLocation(idLocation): Promise<Phong[]> {
    let rooms = await this.prisma.phong.findMany({
      where: {
        ma_vi_tri: idLocation,
      },
    });
    return rooms;
  }

  async getRoomsSearchPagination(
    pageIndex,
    pageSize,
    keyword,
  ): Promise<Phong[]> {
    try {
      if (!pageIndex || !pageSize) {
        throw new HttpException(
          'The page number and number of elements must be greater than 0.',
          400,
        );
      }
      pageIndex = (pageIndex - 1) * pageSize;
      let rooms = await this.prisma.phong.findMany({
        where: {
          ten_phong: {
            contains: keyword,
          },
        },
        skip: pageIndex,
        take: pageSize,
      });
      return rooms;
    } catch (exception) {
      if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getRoomById(id) {
    try {
      let room = await this.prisma.phong.findUnique({
        where: {
          id: id,
        },
      });
      if (!room) {
        throw new HttpException('Resource not found.', 404);
      }
      return room;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async updateRoomById(data, id, body) {
    try {
      let { role } = data;
      if (role !== 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let {
        ten_phong,
        khach,
        phong_ngu,
        giuong,
        phong_tam,
        mo_ta,
        gia_tien,
        may_giat,
        ban_la,
        tivi,
        dieu_hoa,
        wifi,
        bep,
        do_xe,
        ho_boi,
        ban_ui,
        ma_vi_tri,
        hinh_anh,
      } = body;
      let room = await this.prisma.phong.findUnique({
        where: {
          id: id,
        },
      });

      if (!room) {
        throw new HttpException('Resource not found.', 404);
      }

      let location = await this.prisma.viTri.findUnique({
        where: {
          id: ma_vi_tri,
        },
      });
      if (!location) {
        throw new HttpException('"ma-vi-tri" not found..', 404);
      }
      let newRoom = await this.prisma.phong.update({
        where: {
          id: id,
        },
        data: {
          ten_phong,
          khach: khach,
          phong_ngu,
          giuong,
          phong_tam,
          mo_ta,
          gia_tien,
          may_giat,
          ban_la,
          tivi,
          dieu_hoa,
          wifi,
          bep,
          do_xe,
          ho_boi,
          ban_ui,
          ma_vi_tri,
          hinh_anh,
        },
      });

      return newRoom;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async deleteRoomById(data, id) {
    try {
      let { role } = data;
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let room = await this.prisma.phong.findUnique({
        where: {
          id: id,
        },
      });
      if (!room) {
        throw new HttpException('Resource not found.', 404);
      }
      await this.prisma.phong.delete({
        where: {
          id: id,
        },
      });
      return 'Deleted successfully';
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async uploadRoomImage(file, data, id) {
    try {
      let { role } = data;

      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }

      let room = await this.prisma.phong.findUnique({
        where: {
          id: id,
        },
      });
      if (!room) {
        throw new HttpException('Resource not found.', 404);
      }
      let {
        ten_phong,
        khach,
        phong_ngu,
        giuong,
        phong_tam,
        mo_ta,
        gia_tien,
        may_giat,
        ban_la,
        tivi,
        dieu_hoa,
        wifi,
        bep,
        do_xe,
        ho_boi,
        ban_ui,
        ma_vi_tri,
      } = room;
      let updateRoom = await this.prisma.phong.update({
        where: {
          id: id,
        },
        data: {
          ten_phong,
          khach,
          phong_ngu,
          giuong,
          phong_tam,
          mo_ta,
          gia_tien,
          may_giat,
          ban_la,
          tivi,
          dieu_hoa,
          wifi,
          bep,
          do_xe,
          ho_boi,
          ban_ui,
          ma_vi_tri,
          hinh_anh: file?.filename || '',
        },
      });
      return updateRoom;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }
}
