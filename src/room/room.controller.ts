import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { RoomService } from './room.service';
import { Phong } from '@prisma/client';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiProperty,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { UploadDto } from './dto/upload.dto';

class Room {
  @ApiProperty()
  ten_phong: String;
  @ApiProperty()
  khach: Number;
  @ApiProperty()
  phong_ngu: Number;
  @ApiProperty()
  giuong: Number;
  @ApiProperty()
  phong_tam: Number;
  @ApiProperty()
  mo_ta: String;
  @ApiProperty()
  gia_tien: Number;
  @ApiProperty()
  may_giat: Boolean;
  @ApiProperty()
  ban_la: Boolean;
  @ApiProperty()
  tivi: Boolean;
  @ApiProperty()
  dieu_hoa: Boolean;
  @ApiProperty()
  wifi: Boolean;
  @ApiProperty()
  bep: Boolean;
  @ApiProperty()
  do_xe: Boolean;
  @ApiProperty()
  ho_boi: Boolean;
  @ApiProperty()
  ban_ui: Boolean;
  @ApiProperty()
  ma_vi_tri: Number;
  @ApiProperty()
  hinh_anh: String;
}

@ApiTags('Room')
@Controller('room')
export class RoomController {
  constructor(private readonly roomService: RoomService) {}

  @Get('/rooms')
  getRooms(): Promise<Phong[]> {
    return this.roomService.getRooms();
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/room')
  postRoom(@Req() req, @Body() body: Room) {
    let { data } = req.user;
    return this.roomService.postRoom(data, body);
  }

  @Get('/room/get-by-id-location')
  getRoomsByIdLocation(
    @Query('idLocation') idLocation: Number,
  ): Promise<Phong[]> {
    idLocation = Number(idLocation);
    return this.roomService.getRoomsByIdLocation(idLocation);
  }

  @Get('/search-pagination')
  @ApiQuery({ name: 'pageIndex', type: Number, required: false })
  @ApiQuery({ name: 'pageSize', type: Number, required: false })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  getRoomsSearchPagination(
    @Query('pageIndex') pageIndex: Number,
    @Query('pageSize') pageSize: Number,
    @Query('keyword') keyword: string,
  ): Promise<Phong[]> {
    pageIndex = Number(pageIndex);
    pageSize = Number(pageSize);

    return this.roomService.getRoomsSearchPagination(
      pageIndex,
      pageSize,
      keyword,
    );
  }

  @Get('/room/:id')
  getRoomById(@Param('id') id: Number) {
    id = Number(id);
    return this.roomService.getRoomById(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Put('/room/:id')
  updateRoomById(@Req() req, @Param('id') id: Number, @Body() body: Room) {
    let { data } = req.user;
    id = Number(id);
    return this.roomService.updateRoomById(data, id, body);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete('/room/:id')
  deleteRoomById(@Req() req, @Param('id') id: Number) {
    let { data } = req.user;
    id = Number(id);
    return this.roomService.deleteRoomById(data, id);
  }

  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: process.cwd() + '/public/img',
        filename: (req, file, callback) =>
          callback(null, new Date().getTime() + '_' + file.originalname),
      }),
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadDto,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/upload-room-image')
  uploadRoomImage(
    @UploadedFile() file: Express.Multer.File,
    @Req() req,
    @Query('id') id: Number,
  ) {
    let { data } = req.user;
    id = Number(id);

    return this.roomService.uploadRoomImage(file, data, id);
  }
}
