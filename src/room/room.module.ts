import { Module } from '@nestjs/common';
import { RoomService } from './room.service';
import { RoomController } from './room.controller';
import { JwtModule } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [JwtModule, MulterModule],
  controllers: [RoomController],
  providers: [RoomService],
})
export class RoomModule {}
