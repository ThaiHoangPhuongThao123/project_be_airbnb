import { Body, Controller, Delete, Get, Param, Post, Put, Query, Req, UseGuards } from '@nestjs/common';
import { BookingService } from './booking.service';
import { ApiBearerAuth, ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { DatPhong } from '@prisma/client';

class booking {
  @ApiProperty()
  ma_phong: Number;
  @ApiProperty()
  ngay_den: Date;
  @ApiProperty()
  ngay_di: Date;
  @ApiProperty()
  so_luong_khach: Number;
  @ApiProperty()
   ma_nguoi_dat: Number;
}

@ApiTags('Booking')
@Controller('booking')
export class BookingController {
  constructor(private readonly bookingService: BookingService) {}

@ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/bookings')
  getBookings(@Req() req): Promise<DatPhong[]> {
    let { data } = req.user;
    return this.bookingService.getBookings(data);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/booking')
  postBooking(@Req() req, @Body() body: booking) {
    let { data } = req.user;
    return this.bookingService.postBooking(data, body);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/booking/:id')
  getBookingById(
    @Req() req,
    @Param('id') id: Number) {
    let { data } = req.user;
    id = Number(id);
    return this.bookingService.getBookingById(data, id);
  }

   @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Put('/booking/:id')
  updateBookingById(
    @Req() req,
    @Param('id') id: Number,
    @Body() body: booking,
  ) {
    let { data } = req.user;
    id = Number(id);
    return this.bookingService.updateBookingById(data, id, body);
   }
  
   @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete('/user/:id')
  deleteBookingById(@Req() req, @Param('id') id: Number) {
    let { data } = req.user;
    id = Number(id);
    return this.bookingService.deleteBookingById(data, id);
   }
  
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/booking-by-userId/:userId')
  @ApiQuery({ name: 'userId', type: Number, required: false, description: "Parameter is required only when role == 'ADMIN'" })
  getBookingByUserId(
    @Req() req,
    @Query('userId') userId: Number,
) {
    
    let { data } = req.user;
    userId = Number(userId);
    return this.bookingService.getBookingByUserId(data, userId);
  }
}
