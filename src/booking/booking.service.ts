import {
  HttpException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import { DatPhong, PrismaClient } from '@prisma/client';

@Injectable()
export class BookingService {
  prisma = new PrismaClient();

  async getBookings(data): Promise<DatPhong[]> {
    try {
      let { role } = data;
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let bookings = await this.prisma.datPhong.findMany();
      return bookings;
    } catch (exception) {
      if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async postBooking(data, body) {
    try {
      let { ma_phong, ngay_den, ngay_di, so_luong_khach, ma_nguoi_dat } = body;
      if (!data) {
        throw new HttpException('Cannot access the resource ', 400);
      }

      let idRoom = await this.prisma.phong.findFirst({
        where: {
          id: ma_phong,
        },
      });
      if (!idRoom) {
        throw new HttpException('ma_phong not found ', 404);
      }
      if (data.role == "ADMIN" ) {
        let idUser = await this.prisma.nguoiDung.findFirst({
        where: {
          id: ma_nguoi_dat,
        },
      });
      if (!idUser) {
        throw new HttpException('ma_nguoi_dat not found ', 404);
      }

      }
      let newBooking = await this.prisma.datPhong.create({
        data: {
          ma_phong,
          ngay_den,
          ngay_di,
          so_luong_khach,
          ma_nguoi_dat :  data.role == "ADMIN"? ma_nguoi_dat : data.id,
        },
      });
      return newBooking;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getBookingById(data, id) {
    try {
      let { role } = data;

      if (role == 'ADMIN') {
        let booking = await this.prisma.datPhong.findUnique({
          where: {
            id: id,
          },
        });
        if (!booking) {
          throw new HttpException('Resource not found.', 404);
        }
        return booking;
      } else if (role != 'ADMIN') {
        let booking = await this.prisma.datPhong.findUnique({
          where: {
            id: id,
            ma_nguoi_dat: data.id,
          },
        });
        if (!booking) {
          throw new HttpException('Resource not found.', 404);
        }
        return booking;
      } else {
        throw new HttpException('Cannot access the resource', 400);
      }
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async updateBookingById(data, id, body) { 
    try {
      let { role } = data;
      let { ma_phong, ngay_den, ngay_di, so_luong_khach, ma_nguoi_dat } = body;

      if (role == 'ADMIN') {
        let booking = await this.prisma.datPhong.findUnique({
          where: {
            id: id,
          },
        });
        let idRoom = await this.prisma.phong.findFirst({
          where: {
            id: ma_phong,
          },
        });
        if (!idRoom) {
          throw new HttpException('ma_phong not found ', 404);
        }
        let idUser = await this.prisma.nguoiDung.findFirst({
          where: {
            id: ma_nguoi_dat,
          },
        });
        if (!idUser) {
          throw new HttpException('ma_nguoi_dat not found ', 404);
        }

        if (!booking) {
          throw new HttpException('Resource not found.', 404);
        }
        let newBooking = await this.prisma.datPhong.update({
          where: {
            id: id,
          },
          data: {
            ma_phong,
            ngay_den,
            ngay_di,
            so_luong_khach,
            ma_nguoi_dat,
          },
        });
        return newBooking;
      } else if (role != 'ADMIN' ) {
        let booking = await this.prisma.datPhong.findUnique({
          where: {
            id: id,
            ma_nguoi_dat: data.id,
          },
        });

        if (!booking) {
          throw new HttpException('Resource not found.', 404);
        }
        let idRoom = await this.prisma.phong.findFirst({
          where: {
            id: ma_phong,
          },
        });
        if (!idRoom) {
          throw new HttpException('ma_phong not found ', 404);
        }
        let newBooking = await this.prisma.datPhong.update({
          where: {
            id: id,
          },
          data: {
            ma_phong,
            ngay_den,
            ngay_di,
            so_luong_khach,
            ma_nguoi_dat: data.id,
          },
        });

        return newBooking;
      } else {
         throw new HttpException("You are not an ADMIN. You can only update booking your own ID", 403);
      }
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async deleteBookingById(data, id) {
    try {
      let { role } = data;
      if (role == 'ADMIN') {
        let booking = await this.prisma.datPhong.findUnique({
          where: {
            id: id,
          },
        });

        if (!booking) {
          throw new HttpException('Resource not found.', 404);
        }
        await this.prisma.datPhong.delete({
          where: {
            id: id,
          },
        });
        return 'Deleted successfully';
      } else if (role != 'ADMIN') {
        let booking = await this.prisma.datPhong.findUnique({
          where: {
            id: id,
            ma_nguoi_dat: data.id,
          },
        });

        if (!booking) {
          throw new HttpException('Resource not found.', 404);
        }
        await this.prisma.datPhong.delete({
          where: {
            id: id,
          },
        });
        return 'Deleted successfully';
      } else {
                throw new HttpException("You are not an ADMIN. You can only delete booking your own ID", 403);

      }
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getBookingByUserId(data, userId) {
    try {
      let { role } = data;
      // role = AMDIN requires passing parameters through query "userId" to get Booking
      if (role == 'ADMIN') {
        let bookings = await this.prisma.datPhong.findMany({
          where: {
            ma_nguoi_dat: userId,
          },
        });
        if (bookings.length == 0) {
          throw new HttpException('Resource not found.', 404);
        }
        return bookings;
        // role != 'ADMIN' don't need passing parameters through query "userId" because bookings can only be obtained through Data.id
      } else if (role != 'ADMIN') {
        let bookings = await this.prisma.datPhong.findMany({
          where: {
            ma_nguoi_dat: data.id,
          },
        });
        if (bookings.length == 0) {
          throw new HttpException('Resource not found.', 404);
        }
        return bookings;
      } else {
        throw new HttpException("You are not an ADMIN. You can only access your own ID", 403);
      }
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }
}
