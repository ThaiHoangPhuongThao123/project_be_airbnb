import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { NguoiDung, PrismaClient } from '@prisma/client';

@Injectable()
export class AuthService {
  constructor(private jwtService: JwtService) {}
  prisma = new PrismaClient();

  async register(body): Promise<string> {
    let { name, email, pass_word, phone, birth_day, gender, } = body;
     const checkEmail = await this.prisma.nguoiDung.findFirst({
      where: {
        email: email,
      },
     });
    if (checkEmail) {
      
      return 'Email đã tồn tại';
    }

    let newPassword = await bcrypt.hash(pass_word, 10);

    let newUser = {
      name,
      email,
      pass_word: newPassword,
      phone,
      birth_day,
      gender,
      role: 'USER',
      avatar: "",
    }

    await this.prisma.nguoiDung.create({ data: newUser });
    return 'Registered successfully';
  }

  async login(body): Promise<string> {
    let { email, pass_word } = body;
    // check email
    const checkEmail = await this.prisma.nguoiDung.findFirst({
      where: {
        email: email,
      },
    });

    if (checkEmail) {
      const isMatch =
        (await bcrypt.compare(pass_word, checkEmail.pass_word)) ||
        pass_word === checkEmail.pass_word;
      if (isMatch) {
        let token = this.jwtService.sign(
          { data: checkEmail },
          { expiresIn: '10 days', secret: 'BIMAT' },
        );
        return token;
      } else {
        return 'Incorrect password';
      }
    }
    return 'Incorrect email';
  }
}
