import { Body, Controller, Post } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ApiProperty, ApiTags } from '@nestjs/swagger';
import { NguoiDung } from '@prisma/client';

class ModelType  {
   @ApiProperty()
  email: String;
     @ApiProperty()
  pass_word: String;
}

class User  {
   @ApiProperty()
    name: string;
   @ApiProperty()
    email: string;
   @ApiProperty()
    pass_word: string;
   @ApiProperty()
    phone: string;
   @ApiProperty()
    birth_day: string;
   @ApiProperty()
    gender: string;
   //role: string;
}

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  register(@Body() body: User): Promise<string> {
    return this.authService.register(body);
  }

 
  @Post('/login')
  login(@Body() body: ModelType): Promise<string>{
    
    return this.authService.login(body);
  }
}
