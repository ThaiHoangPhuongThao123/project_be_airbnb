import {
  HttpException,
  Injectable,
  InternalServerErrorException,
  NotFoundException,
} from '@nestjs/common';
import { PrismaClient, ViTri } from '@prisma/client';

@Injectable()
export class LocationService {
  prisma = new PrismaClient();
  
  async getLocations(): Promise<ViTri[]> {
    let locations = await this.prisma.viTri.findMany();
    return locations;
  }

  async getLocaltionsSearchPagination(
    pageIndex,
    pageSize,
    keyword,
  ): Promise<ViTri[]> {
    try {
      if (!pageIndex || !pageSize) {
        throw new HttpException('The page number and number of elements must be greater than 0.', 400);
     }
     pageIndex = (pageIndex - 1) * pageSize;
    let locations = await this.prisma.viTri.findMany({
      where: {
        ten_vi_tri: {
          contains: keyword,
        },
      },
      skip: pageIndex,
      take: pageSize,
    });
    return locations;
   } catch (exception) {
    if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
   }
  }

  async postLocation(data, body) {
    try {
      let { role } = data;
      let { ten_vi_tri, tinh_thanh, quoc_gia, hinh_anh } = body;
      if (role !== 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let newLocation = await this.prisma.viTri.create({
        data: {
          ten_vi_tri,
          tinh_thanh,
          quoc_gia,
          hinh_anh,
        },
      });
      return newLocation;
    } catch (exception) {
      if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getLocationById(id) {
    try {
      let location = await this.prisma.viTri.findUnique({
        where: {
          id: id,
        },
      });
      if (!location) {
        throw new HttpException('Resource not found.', 404);
      }
      return location;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async updateLocationById(data, id, body) {
    try {
      let { role } = data;
      if (role !== 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let { ten_vi_tri, tinh_thanh, quoc_gia, hinh_anh } = body;
      let location = await this.prisma.viTri.findUnique({
        where: {
          id: id,
        },
      });

      if (!location) {
        throw new HttpException('Resource not found.', 404);
      }
      let newLocation = await this.prisma.viTri.update({
        where: {
          id: id,
        },
        data: {
          ten_vi_tri: ten_vi_tri,
          tinh_thanh: tinh_thanh,
          quoc_gia: quoc_gia,
          hinh_anh: hinh_anh,
        },
      });

      return newLocation;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async deleteLocationById(data, id) {
    try {
      let { role } = data;
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let location = await this.prisma.viTri.findUnique({
        where: {
          id: id,
        },
      });
      if (!location) {
        throw new HttpException('Resource not found.', 404);
      }
      await this.prisma.viTri.delete({
        where: {
          id: id,
        },
      });
      return 'Deleted successfully';
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

 async uploadLocationImage(file, data, id) {
    try {
      let { role  } = data;
      
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
       
      let location = await this.prisma.viTri.findUnique({
        where: {
          id: id,
        },
      });
      if (!location) {
        throw new HttpException('Resource not found.', 404);
      }
  let { ten_vi_tri, tinh_thanh, quoc_gia } = location;
    let updateLocation=  await this.prisma.viTri.update({
        where: {
          id: id,
        },
        data: {
          ten_vi_tri: ten_vi_tri,
          tinh_thanh: tinh_thanh,
          quoc_gia: quoc_gia,
          hinh_anh: file?.filename || "" ,
        },
    });
      return updateLocation;
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    
    }
  }
}
