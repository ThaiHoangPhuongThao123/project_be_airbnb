import {
  Body,
  Controller,
  Delete,
  Get,
  Headers,
  Param,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { LocationService } from './location.service';
import {
  ApiBearerAuth,
  ApiBody,
  ApiConsumes,
  ApiProperty,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger';
import { AuthGuard } from '@nestjs/passport';
import { ViTri } from '@prisma/client';
import { UploadDto } from './dto/upload.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';

class location {
  @ApiProperty()
  ten_vi_tri: String;
  @ApiProperty()
  tinh_thanh: String;
  @ApiProperty()
  quoc_gia: String;
  @ApiProperty()
  hinh_anh: String;
}

@ApiTags('Location')
@Controller('location')
export class LocationController {
  constructor(private readonly locationService: LocationService) {}

  @Get('/locations')
  getLocations(): Promise<ViTri[]> {
    return this.locationService.getLocations();
  }

  @Get('/search-pagination')
  @ApiQuery({ name: 'pageIndex', type: Number, required: false })
  @ApiQuery({ name: 'pageSize', type: Number, required: false })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  getLocaltionsSearchPagination(
    @Query('pageIndex') pageIndex: Number,
    @Query('pageSize') pageSize: Number,
    @Query('keyword') keyword: string,
  ): Promise<ViTri[]> {
    pageIndex = Number(pageIndex);
    pageSize = Number(pageSize);

    return this.locationService.getLocaltionsSearchPagination(
      pageIndex,
      pageSize,
      keyword,
    );
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/location')
  postLocation(@Req() req, @Body() body: location) {
    let { data } = req.user;
    return this.locationService.postLocation(data, body);
  }

  @Get('/location/:id')
  getLocationById(@Param('id') id: Number) {
    id = Number(id);
    return this.locationService.getLocationById(id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Put('/location/:id')
  updateLocationById(
    @Req() req,
    @Param('id') id: Number,
    @Body() body: location,
  ) {
    let { data } = req.user;
    id = Number(id);
    return this.locationService.updateLocationById(data, id, body);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete('/location/:id')
  deleteLocationById(@Req() req, @Param('id') id: Number) {
    let { data } = req.user;
    id = Number(id);
    return this.locationService.deleteLocationById(data, id);
  }


  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: process.cwd() + '/public/img',
        filename: (req, file, callback) =>
          callback(null, new Date().getTime() + '_' + file.originalname),
      }),
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadDto,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/upload-location-image')
  uploadLocationImage(
    @UploadedFile() file: Express.Multer.File,
    @Req() req,
    @Query('id') id: Number,
  ) {
    let { data } = req.user;
    id = Number(id);

    return this.locationService.uploadLocationImage(file, data, id);
  }
}
