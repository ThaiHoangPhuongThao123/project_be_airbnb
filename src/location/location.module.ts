import { Module } from '@nestjs/common';
import { LocationService } from './location.service';
import { LocationController } from './location.controller';
import { JwtModule } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [JwtModule, MulterModule],
  controllers: [LocationController],
  providers: [LocationService],
})
export class LocationModule {}
