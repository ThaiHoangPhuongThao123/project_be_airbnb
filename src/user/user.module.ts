import { Module } from '@nestjs/common';
import { UserService } from './user.service';
import { UserController } from './user.controller';
import { JwtModule } from '@nestjs/jwt';
import { MulterModule } from '@nestjs/platform-express';

@Module({
  imports: [JwtModule, MulterModule],
  controllers: [UserController],
  providers: [UserService],
})
export class UserModule {}
