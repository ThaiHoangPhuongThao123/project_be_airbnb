import {
  HttpException,
  Injectable,
  InternalServerErrorException,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { NguoiDung, PrismaClient } from '@prisma/client';

@Injectable()
export class UserService {
  prisma = new PrismaClient();

  async getUsers(data): Promise<NguoiDung[]> {
    try {
      let { role } = data;
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let users = await this.prisma.nguoiDung.findMany();
      return users;
    } catch (exception) {
      if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async postUser(data, body) {
    try {
      let { role } = data;
      let { name, email, pass_word, phone, birth_day, gender, avatar } = body;
      if (role !== 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }

      const checkEmail = await this.prisma.nguoiDung.findFirst({
        where: {
          email: email,
        },
      });
      if (checkEmail) {
        throw new HttpException('Email already exists', 400);
      }
      let newPassword = await bcrypt.hash(pass_word, 10);
      let newUser = await this.prisma.nguoiDung.create({
        data: {
          name,
          email,
          pass_word: newPassword,
          phone,
          birth_day,
          gender,
          role: body.role || 'USER',
          avatar: avatar,
        },
      });
      return newUser;
    } catch (exception) {
      if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async deleteUserById(data, id) {
    try {
      let { role } = data;
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      let user = await this.prisma.nguoiDung.findUnique({
        where: {
          id: id,
        },
      });
      if (!user) {
        throw new HttpException('Resource not found.', 404);
      }
      await this.prisma.nguoiDung.delete({
        where: {
          id: id,
        },
      });
      return 'Deleted successfully';
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getUsersSearchPagination(
    data,
    pageIndex,
    pageSize,
    keyword,
  ): Promise<NguoiDung[]> { 
    try {
      let { role } = data;
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      if (!pageIndex || !pageSize) {
        throw new HttpException(
          'The page number and number of elements must be greater than 0.',
          400,
        );
      }
      pageIndex = (pageIndex - 1) * pageSize;
      let users = await this.prisma.nguoiDung.findMany({
        where: {
          name: {
            contains: keyword,
          },
        },
        skip: pageIndex,
        take: pageSize,
      });
      return users;
    } catch (exception) {
      if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getUserById(data, id) {
    try {
      let { role } = data;
      
      if (role == "ADMIN" ) {
        let user = await this.prisma.nguoiDung.findUnique({
        where: {
          id: id,
        },
      });
      if (!user) {
        throw new HttpException('Resource not found.', 404);
      }
      return user;
      } else if (role != "ADMIN" && id == data.id) {
        let user = await this.prisma.nguoiDung.findUnique({
        where: {
          id: data.id,
        },
      });
      if (!user) {
        throw new HttpException('Resource not found.', 404);
      }
      return user;
      } else {
        throw new HttpException('Cannot access the resource', 400);
      }
      
    } catch (exception) {
      if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      }else if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async updateUserById(data, id, body) {
    try {
      let { role } = data;
      let { name, email, pass_word, phone, birth_day, gender, avatar } = body;
      
      if (role == "ADMIN" ) {
      let user = await this.prisma.nguoiDung.findUnique({
        where: {
          id: id,
        },
      });

      if (!user) {
        throw new HttpException('Resource not found.', 404);
      }
        const checkEmail = await this.prisma.nguoiDung.findFirst({
          where: {
            email: email,
            NOT: {
              id: id,
            }
        },
      });
      if (checkEmail) {
        throw new HttpException('Email already exists', 400);
      }
      let newPassword = await bcrypt.hash(pass_word, 10);
      let newUser = await this.prisma.nguoiDung.update({
        where: {
          id: id,
        },
        data: {
          name,
          email,
          pass_word: newPassword,
          phone,
          birth_day,
          gender,
          role: body?.role ,
          avatar: avatar,
        },
      });
      return newUser;
      } else if (role != "ADMIN" && id == data.id) {
        let user = await this.prisma.nguoiDung.findUnique({
        where: {
          id: data.id,
        },
      });

      if (!user) {
        throw new HttpException('Resource not found.', 404);
      }
      let newPassword = await bcrypt.hash(pass_word, 10);
      let newUser = await this.prisma.nguoiDung.update({
        where: {
          id: data.id,
        },
        data: {
          name,
          email,
          pass_word: newPassword,
          phone,
          birth_day,
          gender,
          role: "USER" ,
          avatar: avatar,
        },
      });
        
        
      return newUser;
      } else {
        throw new HttpException('Cannot access the resource', 400);
      }
    } catch (exception) {
      if (exception.status === 404) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
    }
  }

  async getUserSearchUserName(
    data,
    userName
  ): Promise<NguoiDung[]> {
    try {
      let { role  } = data;
      
      if (role != 'ADMIN') {
        throw new HttpException('Forbidden, Must be ADMIN to access ', 403);
      }
      if (userName.trim() == "") {
        throw new HttpException('Please enter user name you want to search', 400);
     }
    let users = await this.prisma.nguoiDung.findMany({
      where: {
        name: {
          contains: userName,
        },
      },
    });
    return users;
   } catch (exception) {
    if (exception.status === 400) {
        throw new HttpException(exception.response, exception.status);
      } else if (exception.status === 403) {
        throw new HttpException(exception.response, exception.status);
      }
      throw new InternalServerErrorException('An unexpected error occurred');
   }
  }

  async uploadAvatar(file, data) {
      
  let {id, name, email, pass_word, phone, birth_day, gender, } = data;
    let updateUser=  await this.prisma.nguoiDung.update({
        where: {
          id: id,
        },
        data: {
          name, email, pass_word, phone, birth_day, gender,
          avatar: file?.filename || "" ,
        },
    });
      return updateUser;
   
  }
}
