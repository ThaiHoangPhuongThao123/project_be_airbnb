import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  Req,
  UploadedFile,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiBearerAuth, ApiBody, ApiConsumes, ApiProperty, ApiQuery, ApiTags } from '@nestjs/swagger';
import { NguoiDung } from '@prisma/client';
import { AuthGuard } from '@nestjs/passport';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { UploadDto } from './dto/upload.dto';

class user {
  @ApiProperty()
  name: String;
  @ApiProperty()
  email: String;
  @ApiProperty()
  pass_word: String;
  @ApiProperty()
  phone: String;
  @ApiProperty()
  birth_day: String;
  @ApiProperty()
  gender: String;
  @ApiProperty()
  role: String;
  @ApiProperty()
  avatar: String;
}

@ApiTags('User')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/users')
  getUsers(@Req() req): Promise<NguoiDung[]> {
    let { data } = req.user;
    return this.userService.getUsers(data);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/user')
  postUser(@Req() req, @Body() body: user) {
    let { data } = req.user;
    return this.userService.postUser(data, body);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Delete('/user/:id')
  deleteUserById(@Req() req, @Param('id') id: Number) {
    let { data } = req.user;
    id = Number(id);
    return this.userService.deleteUserById(data, id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/search-pagination')
  @ApiQuery({ name: 'pageIndex', type: Number, required: false })
  @ApiQuery({ name: 'pageSize', type: Number, required: false })
  @ApiQuery({ name: 'keyword', type: String, required: false })
  getUsersSearchPagination(
    @Req() req,
    @Query('pageIndex') pageIndex: Number,
    @Query('pageSize') pageSize: Number,
    @Query('keyword') keyword: string,
  ): Promise<NguoiDung[]> {
    let { data } = req.user;
    pageIndex = Number(pageIndex);
    pageSize = Number(pageSize);

    return this.userService.getUsersSearchPagination(
      data,
      pageIndex,
      pageSize,
      keyword,
    );
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/user/:id')
  getUserById(
    @Req() req,
    @Param('id') id: Number) {
    let { data } = req.user;
    id = Number(id);
    return this.userService.getUserById(data, id);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Put('/user/:id')
  updateUserById(
    @Req() req,
    @Param('id') id: Number,
    @Body() body: user,
  ) {
    let { data } = req.user;
    id = Number(id);
    return this.userService.updateUserById(data, id, body);
  }

  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Get('/search/:userName')
  getUserSearchUserName(
    @Req() req,
    @Param('userName') userName: String, 
  ): Promise<NguoiDung[]> {
    let { data } = req.user;
    return this.userService.getUserSearchUserName(
      data,
      userName
    );
  }

  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: process.cwd() + '/public/img',
        filename: (req, file, callback) =>
          callback(null, new Date().getTime() + '_' + file.originalname),
      }),
    }),
  )
  @ApiConsumes('multipart/form-data')
  @ApiBody({
    type: UploadDto,
  })
  @ApiBearerAuth()
  @UseGuards(AuthGuard('jwt'))
  @Post('/upload-avatar')
  uploadAvatar(
    @UploadedFile() file: Express.Multer.File,
    @Req() req,
  ) {
    let { data } = req.user;


    return this.userService.uploadAvatar(file, data);
  }
}
