import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { LocationModule } from './location/location.module';
import { JwtStrategy } from './strategy/jwt.stategy';
import { ConfigModule } from '@nestjs/config';
import { RoomModule } from './room/room.module';
import { UserModule } from './user/user.module';
import { BookingModule } from './booking/booking.module';
import { CommentModule } from './comment/comment.module';

@Module({
  imports: [AuthModule, LocationModule, ConfigModule.forRoot({
    isGlobal:true,
  }), RoomModule, UserModule, BookingModule, CommentModule],
  controllers: [AppController],
  providers: [AppService, JwtStrategy],
})
export class AppModule {}
