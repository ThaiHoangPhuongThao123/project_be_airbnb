/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

CREATE TABLE `BinhLuan` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ma_phong` int DEFAULT NULL,
  `ma_nguoi_binh_luan` int DEFAULT NULL,
  `ngay_binh_luan` datetime DEFAULT NULL,
  `noi_dung` text,
  `sao_binh_luan` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ma_phong` (`ma_phong`),
  KEY `ma_nguoi_binh_luan` (`ma_nguoi_binh_luan`),
  CONSTRAINT `BinhLuan_ibfk_1` FOREIGN KEY (`ma_phong`) REFERENCES `Phong` (`id`),
  CONSTRAINT `BinhLuan_ibfk_2` FOREIGN KEY (`ma_nguoi_binh_luan`) REFERENCES `NguoiDung` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `DatPhong` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ma_phong` int DEFAULT NULL,
  `ngay_den` datetime DEFAULT NULL,
  `ngay_di` datetime DEFAULT NULL,
  `so_luong_khach` int DEFAULT NULL,
  `ma_nguoi_dat` int DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ma_phong` (`ma_phong`),
  KEY `ma_nguoi_dat` (`ma_nguoi_dat`),
  CONSTRAINT `DatPhong_ibfk_1` FOREIGN KEY (`ma_phong`) REFERENCES `Phong` (`id`),
  CONSTRAINT `DatPhong_ibfk_2` FOREIGN KEY (`ma_nguoi_dat`) REFERENCES `NguoiDung` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `NguoiDung` (
  `id` int NOT NULL AUTO_INCREMENT,
  `name` varchar(400) DEFAULT NULL,
  `email` varchar(400) DEFAULT NULL,
  `pass_word` varchar(400) DEFAULT NULL,
  `phone` varchar(400) DEFAULT NULL,
  `birth_day` varchar(400) DEFAULT NULL,
  `gender` varchar(400) DEFAULT NULL,
  `role` varchar(400) DEFAULT NULL,
  `avatar` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `Phong` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_phong` varchar(400) DEFAULT NULL,
  `khach` int DEFAULT NULL,
  `phong_ngu` int DEFAULT NULL,
  `giuong` int DEFAULT NULL,
  `phong_tam` int DEFAULT NULL,
  `mo_ta` text,
  `gia_tien` int DEFAULT NULL,
  `may_giat` tinyint(1) DEFAULT NULL,
  `ban_la` tinyint(1) DEFAULT NULL,
  `tivi` tinyint(1) DEFAULT NULL,
  `dieu_hoa` tinyint(1) DEFAULT NULL,
  `wifi` tinyint(1) DEFAULT NULL,
  `bep` tinyint(1) DEFAULT NULL,
  `do_xe` tinyint(1) DEFAULT NULL,
  `ho_boi` tinyint(1) DEFAULT NULL,
  `ban_ui` tinyint(1) DEFAULT NULL,
  `ma_vi_tri` int DEFAULT NULL,
  `hinh_anh` text,
  PRIMARY KEY (`id`),
  KEY `ma_vi_tri` (`ma_vi_tri`),
  CONSTRAINT `Phong_ibfk_1` FOREIGN KEY (`ma_vi_tri`) REFERENCES `ViTri` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

CREATE TABLE `ViTri` (
  `id` int NOT NULL AUTO_INCREMENT,
  `ten_vi_tri` varchar(400) DEFAULT NULL,
  `tinh_thanh` varchar(400) DEFAULT NULL,
  `quoc_gia` varchar(400) DEFAULT NULL,
  `hinh_anh` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `BinhLuan` (`id`, `ma_phong`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`) VALUES
(1, 2, 2, '2023-05-06 10:10:10', 'nhẹ nhàng và tinh tế.', 4);
INSERT INTO `BinhLuan` (`id`, `ma_phong`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`) VALUES
(2, 3, 1, '2023-02-04 10:10:10', 'ánh sáng tự nhiên cho căn phòng.', 3);
INSERT INTO `BinhLuan` (`id`, `ma_phong`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`) VALUES
(3, 2, 1, '2023-05-06 10:10:10', 'Ok.', 2);
INSERT INTO `BinhLuan` (`id`, `ma_phong`, `ma_nguoi_binh_luan`, `ngay_binh_luan`, `noi_dung`, `sao_binh_luan`) VALUES
(4, 3, 4, '2023-05-06 10:10:10', 'rất đẹp', 5),
(6, 2, 2, '2023-12-25 03:28:48', 'hjdfbjdhbfjdbjhfdbj', 5),
(7, 2, 7, '2023-12-25 03:57:04', 'DSJHGFJHSFJ', 4),
(8, 3, 7, '2023-12-25 04:29:50', 'hi', 3);

INSERT INTO `DatPhong` (`id`, `ma_phong`, `ngay_den`, `ngay_di`, `so_luong_khach`, `ma_nguoi_dat`) VALUES
(1, 2, '2023-01-01 10:10:10', '2023-01-03 10:10:10', 3, 2);
INSERT INTO `DatPhong` (`id`, `ma_phong`, `ngay_den`, `ngay_di`, `so_luong_khach`, `ma_nguoi_dat`) VALUES
(2, 3, '2023-05-01 10:10:10', '2023-06-03 10:10:10', 4, 1);
INSERT INTO `DatPhong` (`id`, `ma_phong`, `ngay_den`, `ngay_di`, `so_luong_khach`, `ma_nguoi_dat`) VALUES
(3, 3, '2023-05-02 10:10:10', '2023-06-05 10:10:10', 3, 2);
INSERT INTO `DatPhong` (`id`, `ma_phong`, `ngay_den`, `ngay_di`, `so_luong_khach`, `ma_nguoi_dat`) VALUES
(4, 3, '2023-12-20 11:08:15', '2023-12-20 11:08:15', 2, 1),
(5, 3, '2023-12-20 11:08:15', '2023-12-20 11:08:15', 2, 1),
(6, 4, '2023-12-21 07:22:00', '2023-12-21 07:22:00', 5, 1),
(9, 2, '2023-12-21 08:11:56', '2023-12-21 08:11:56', 15, 3),
(11, 4, '2023-12-21 07:22:00', '2023-12-21 07:22:00', 5000, 2),
(14, 2, '2023-12-21 10:32:17', '2023-12-21 10:32:17', 5, 1),
(15, 1, '2023-12-22 06:52:14', '2023-12-22 06:52:14', 2, 3),
(16, 2, '2023-12-22 06:37:24', '2023-12-22 06:37:24', 2, 2),
(18, 2, '2023-12-25 03:03:33', '2023-12-25 03:03:33', 1, 7);

INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `avatar`) VALUES
(1, 'string', 'emeli@gamil.com', '$2b$10$DlqSNdUPrQ64b00wTE/q3.Ugao1V7uN5cpxBB48WLhU/ZGSCxlGye', '0214565', '07-07-18888', 'female', 'USER', '1703052966468_conan_avatar.png');
INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `avatar`) VALUES
(2, 'emeli1', 'emeli1@gamil.com', 'emeli', '02154635', '2005-01-01', 'female', 'USER', '');
INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `avatar`) VALUES
(3, 'emeli2', 'emeli2@gamil.com', 'emeli', '02154635', '2005-01-01', 'female', 'USER', '');
INSERT INTO `NguoiDung` (`id`, `name`, `email`, `pass_word`, `phone`, `birth_day`, `gender`, `role`, `avatar`) VALUES
(4, 'emeli3', 'emeli3@gamil.com', 'emeli', '02154635', '2005-01-01', 'female', 'USER', ''),
(5, 'emeli4', 'emeli4@gamil.com', 'emeli', '02154635', '2005-01-01', 'female', 'USER', ''),
(6, 'tien hung', 'tienhung@gamil.com', 'tienhung', '09853214', '2002-06-05', 'male', 'ADMIN', '1703052923778_conan_avatar.png'),
(7, 'string', 'string', '$2b$10$S3.5AwRebBfFuF7zyA99T.wPlpk9D3NWxNoFUszGaNNJWZo1fwLqG', 'string', 'string', 'string', 'USER', '1703481666205_conan_avatar.png'),
(8, 'string string', 'string', '$2b$10$cB4xyfiLn2yJ0RQnV8rGQO7j1lJspFb9GgCtFS6k.S3p1dRzoWCVm', 'string', 'string', 'string', 'USER', 'string'),
(9, 'string', 'string5555', '$2b$10$XSTKG87.7DUy2tr/6/IOuuFk3LGy1/NhMODJpvbPC/b1bGyGnlZly', 'string', 'string', 'string', 'string', 'string'),
(14, 'string', 'string1234', '$2b$10$awI.OaqCzQifnVQwF38mNuoNdgxt1HRF8/yiMsnAHUouttphEaHzy', 'string', 'string', 'string', 'string', ''),
(24, 'abc', 'abc@gmail.com', '$2b$10$hZbmyPT6rO0n7CG/WojD/ud9n7dQ3xpgLFyRHI/9vCPpIVM1jHVjO', '01223654', '02/02/2002', 'male', 'USER', ''),
(25, 'mimi111', 'mimi@gmail.com', '$2b$10$ZF.aQ/NtrMGCSLViebXI2.Gzocn3UZO7Mz8fQ5w8Oqf5gEk1qn6Xy', '0444444', '03/03/2006', 'male', 'USER', '');

INSERT INTO `Phong` (`id`, `ten_phong`, `khach`, `phong_ngu`, `giuong`, `phong_tam`, `mo_ta`, `gia_tien`, `may_giat`, `ban_la`, `tivi`, `dieu_hoa`, `wifi`, `bep`, `do_xe`, `ho_boi`, `ban_ui`, `ma_vi_tri`, `hinh_anh`) VALUES
(1, ' Căn hộ chung cư Keangnam Apartment', 3, 2, 2, 1, 'Đây là một trong những căn hộ chung cư đẹp nhất Việt Nam được đánh giá cao với phong cách đậm tính tối giản, hiện đại.', 18, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 'https://img.homedy.com/store/images/2020/04/09/can-ho-keangnam-637220481937858519.jpg');
INSERT INTO `Phong` (`id`, `ten_phong`, `khach`, `phong_ngu`, `giuong`, `phong_tam`, `mo_ta`, `gia_tien`, `may_giat`, `ban_la`, `tivi`, `dieu_hoa`, `wifi`, `bep`, `do_xe`, `ho_boi`, `ban_ui`, `ma_vi_tri`, `hinh_anh`) VALUES
(2, 'Căn hộ chung cư cao cấp Golden Westlake', 6, 3, 3, 2, ' Dự án được đánh giá cao do xây dựng trên mảnh đất vuông vắn, giao thông thuận tiện, từ vị trí của tòa nhà có thể nhìn thẳng ra Hồ Tây.', 30, 1, 0, 1, 1, 1, 1, 1, 0, 0, 2, 'https://img.homedy.com/store/images/2020/04/09/golden-westlake-637220495823818179.jpg');
INSERT INTO `Phong` (`id`, `ten_phong`, `khach`, `phong_ngu`, `giuong`, `phong_tam`, `mo_ta`, `gia_tien`, `may_giat`, `ban_la`, `tivi`, `dieu_hoa`, `wifi`, `bep`, `do_xe`, `ho_boi`, `ban_ui`, `ma_vi_tri`, `hinh_anh`) VALUES
(3, 'Vinhomes Central Park ', 5, 3, 3, 2, ' Có thể nói Vinhomes Central Park chính là khu căn hộ sang trọng bậc nhất tại Việt Nam với nhiều ưu điểm vượt trội.', 28, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 'https://img.homedy.com/store/images/2020/04/09/vinhomes-central-park-637220496383173588.jpg');
INSERT INTO `Phong` (`id`, `ten_phong`, `khach`, `phong_ngu`, `giuong`, `phong_tam`, `mo_ta`, `gia_tien`, `may_giat`, `ban_la`, `tivi`, `dieu_hoa`, `wifi`, `bep`, `do_xe`, `ho_boi`, `ban_ui`, `ma_vi_tri`, `hinh_anh`) VALUES
(4, 'Căn hộ khu dân cư Tấn Trường ', 4, 2, 2, 2, ' Nằm tại khu dân cư phát triển gần trung tâm quận 7, căn hộ Tấn Trường không chỉ có vị trí thuận tiện mà còn có nhiều tiện ích đi kèm như sân chơi trẻ em, chỗ nướng BBQ, hồ bơi, sân Tennis,... ', 21, 1, 0, 1, 0, 1, 1, 1, 1, 0, 3, 'https://img.homedy.com/store/images/2020/04/09/tan-truong-637220497139261290.jpg'),
(5, 'ting tign ', 5, 2, 2, 1, 'string', 0, 1, 1, 0, 1, 0, 1, 1, 1, 1, 2, '1703220807299_conan_avatar.png'),
(7, 'giaugiau', 5, 2, 2, 2, 'string', 18, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 'string'),
(8, 'amix123', 3, 2, 2, 1, 'string', 10, 1, 0, 1, 1, 1, 1, 1, 1, 1, 2, '1703476225086_conan_avatar.png');

INSERT INTO `ViTri` (`id`, `ten_vi_tri`, `tinh_thanh`, `quoc_gia`, `hinh_anh`) VALUES
(1, 'Quận 7', 'tp.Hồ Chí Minh', 'Việt Nam', 'https://www.google.com/imgres?imgurl=https%3A%2F%2Fbachkhoaland.com%2Fwp-content%2Fuploads%2F2021%2F04%2Fquan-7-thanh-pho-ho-chi-minh.jpg&tbnid=lXYI3bjNxKvgzM&vet=12ahUKEwihv6GtyPyCAxUTmVYBHdATAqwQMygBegQIARBH..i&imgrefurl=https%3A%2F%2Fbachkhoaland.com%2Ftat-tan-tat-ve-quan-7-thanh-pho-ho-chi-minh%2F&docid=BQbmtFO0fvfP8M&w=800&h=450&q=qu%E1%BA%ADn%207&ved=2ahUKEwihv6GtyPyCAxUTmVYBHdATAqwQMygBegQIARBH');
INSERT INTO `ViTri` (`id`, `ten_vi_tri`, `tinh_thanh`, `quoc_gia`, `hinh_anh`) VALUES
(2, 'Bình Thạnh', 'tp.Hồ Chí Minh', 'Việt Nam', 'https://media.urbanistnetwork.com/saigoneer/article-images/2020/10/08/binhthanh0h.jpg');
INSERT INTO `ViTri` (`id`, `ten_vi_tri`, `tinh_thanh`, `quoc_gia`, `hinh_anh`) VALUES
(3, 'Hai Bà Trưng', 'Hà Nội', 'Việt Nam', 'https://fpttoanquoc.com/uploads/news/hanoi/hoankiem-hbt/lap-mang-fpt-hai-ba-trung.png');
INSERT INTO `ViTri` (`id`, `ten_vi_tri`, `tinh_thanh`, `quoc_gia`, `hinh_anh`) VALUES
(5, 'string22', 'string22', 'string22', '1703478181519_conan_avatar.png'),
(7, 'string55', 'string55', 'string55', 'string55');


/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;